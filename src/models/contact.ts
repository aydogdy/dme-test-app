export class Contact {

    
    phone: string;
    email: string;
    name: Name;
    picture: Pic;

    constructor(phone: string,
                email: string,
                name: Name,
                picture: Pic){
      
        
        this.phone = phone;
        this.email = email;
        this.name = name;
        this.picture = picture;
        
    }

}

class Name {
    title: string;
    first: string;
    last: string;
}

class Pic {
    large: string;
    medium: string;
    thumbnail: string;
}