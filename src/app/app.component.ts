import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ContactsPage } from '../pages/contacts/contacts';
import { SettingsProvider } from '../pages/providers/settings';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = ContactsPage;
  selectedTheme: String;

  constructor(platform: Platform,
              statusBar: StatusBar,
              private settings: SettingsProvider,
              splashScreen: SplashScreen) {

    this.settings.getActiveTheme().subscribe(val => this.selectedTheme = val);

    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

