import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { CallNumber } from '@ionic-native/call-number';
import { SMS } from '@ionic-native/sms';
import { IonicImageViewerModule } from 'ionic-img-viewer';

import { MyApp } from './app.component';
import { ContactsPageModule } from '../pages/contacts/contacts.module';
import { ContactInfoPageModule } from '../pages/contact-info/contact-info.module';
import { ContactsService } from '../services/contacts-service';
import { ConfigService } from '../services/config-service';
import { HttpModule } from '@angular/http';
import { SettingsProvider } from '../pages/providers/settings';

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicImageViewerModule,
    ContactsPageModule,
    ContactInfoPageModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SettingsProvider,
    ContactsService,
    ConfigService,
    SplashScreen,
    PhotoViewer,
    CallNumber,
    SMS,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
