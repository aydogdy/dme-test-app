import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class SettingsProvider {
 
    private theme: BehaviorSubject<String>;
    selectedTheme: String;

    constructor() {
        let dme = JSON.parse(localStorage.getItem("dme"));
        if(dme && dme.mode && dme.mode !== ""){
            this.selectedTheme = dme["mode"];
        }else{
            this.selectedTheme = "light-theme";
            let t = {"token": "", "mode": null};
            t["mode"] = this.selectedTheme;
            localStorage.setItem('dme', JSON.stringify(t));
        }
        this.theme = new BehaviorSubject(this.selectedTheme);
    }
 
    setActiveTheme(val) {
        let dme = JSON.parse(localStorage.getItem("dme"));
        dme.mode = val;
        localStorage.setItem('dme', JSON.stringify(dme));
        this.theme.next(val);
    }
 
    getActiveTheme() {
        return this.theme.asObservable();
    }
}
