import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ContactsService } from '../../services/contacts-service';
import { Contact } from '../../models/contact';
import { ContactInfoPage } from '../contact-info/contact-info';
import { ConfigService } from '../../services/config-service';
import { SettingsProvider } from '../providers/settings';

@IonicPage()
@Component({
  selector: 'page-contacts',
  templateUrl: 'contacts.html',
})
export class ContactsPage {

  selectedTheme: String;
  contacts: Array<Contact> = [];
  searchQuery: string = '';
  searchItemIsFound: boolean = true;
  sele

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private configService: ConfigService,
              private settings: SettingsProvider,
              private contactsService: ContactsService) {

      this.settings.getActiveTheme().subscribe(val => this.selectedTheme = val);

      this.contactsService.getMyCollegues(true).then((res: any)=>{
        this.contacts = this.contactsService.contacts;
          this.contacts.length > 0 ? this.searchItemIsFound=true : this.searchItemIsFound = false;
      }, error => {
        console.log("Error");
      });
  }


    toggleAppTheme() {
        if (this.selectedTheme === 'dark-theme') {
          this.settings.setActiveTheme('light-theme');
        } else {
          this.settings.setActiveTheme('dark-theme');
        }
    }

    doRefresh(refresher) {
        this.contactsService.getMyCollegues(false).then((res: any)=>{
          this.contacts = this.contactsService.contacts;
          this.contacts.length > 0 ? this.searchItemIsFound=true : this.searchItemIsFound = false;
          this.searchQuery = "";
          refresher.complete();
        }, error=>{
          refresher.complete();
        })
    }

    onSelectContact(contact: Contact){
      this.navCtrl.push(ContactInfoPage, {contact});
    }

    getContacts(ev: any) {
      this.contacts = this.contactsService.contacts;
      this.searchQuery = ev.target.value;
      if (this.searchQuery && this.searchQuery.trim() != '') {
        this.contacts = this.contacts.filter((contact) => {
          this.searchItemIsFound = contact.name.first.toLowerCase().indexOf(this.searchQuery.toLowerCase()) > -1;
          return this.searchItemIsFound;
        })
      }
    }

    deleteContact(indx: number){
     this.contacts.splice(indx, 1);
     this.configService.presentToast("Контакт удален!", "success", "bottom", false, "", 2000);
    }

}
