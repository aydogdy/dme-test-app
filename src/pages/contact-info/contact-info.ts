import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ActionSheetController } from 'ionic-angular';
import { Contact } from '../../models/contact';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { CallNumber } from '@ionic-native/call-number';
import { SMS } from '@ionic-native/sms';
import { ConfigService } from '../../services/config-service';
import { ImageViewerController } from 'ionic-img-viewer';

@IonicPage()
@Component({
  selector: 'page-contact-info',
  templateUrl: 'contact-info.html',
})
export class ContactInfoPage {

  contact: Contact;
  imageViewer: ImageViewerController;
  
  constructor(public navCtrl: NavController,
              private photoViewer: PhotoViewer,
              private callNumber: CallNumber,
              private sms: SMS,
              public alertCtrl: AlertController,
              private contactsService: ConfigService,
              public actionSheetCtrl: ActionSheetController,
              private imageViewerCtrl: ImageViewerController,
              public navParams: NavParams) {

      this.contact = this.navParams.get("contact");
      this.imageViewer = this.imageViewerCtrl;
  }

  //View on full screnn contact image
  onViewPhoto(myImage){
      const imageViewer = this.imageViewer.create(myImage);
      imageViewer.present(); 
  }

  callAndSmsActionSheet(){
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Выберите действие',
      buttons: [
        {
          text: 'Позвонить',
          icon: 'phone-portrait',
          cssClass: 'editionIcon',
          role: 'destructive',
          handler: () => {
            this.callToContact();
          }
        },
        {
          text: 'Написать СМС',
          icon: 'chatboxes',
          cssClass: 'editionIcon',
          handler: () => {
            this.showSmsPrompt();
          }
        },
        {
          text: 'Отмена',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
 
    actionSheet.present();
  }

  //Calling plugin to call
  callToContact(){
    this.callNumber.callNumber(this.contact.phone, true)
    .then(() => console.log('Launched dialer!'))
    .catch(() => console.log('Error launching dialer'));
  }

  //Sms prompt to sent SMS
  showSmsPrompt() {
    let prompt = this.alertCtrl.create({
      title: 'СМС',
      message: "Напишите текст смс",
      inputs: [
        {
          name: 'smstext',
          placeholder: 'Привет...'
        },
      ],
      buttons: [
        {
          text: 'Отмена',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Отправить',
          handler: data => {
            if(data.smstext != ''){
              this.sendSms(data.smstext);
            }else{
              this.contactsService.presentToast("Вы не можете отправить пустое сообщение!", "error", "bottom", false, "", 3000);
            }
          }
        }
      ]
    });
    prompt.present();
  }

  //Calling plugin to sent sms
  sendSms(text: string){
    this.sms.send(this.contact.phone, text);
    this.contactsService.presentToast("Смс успешно отправлено!", "success", "bottom", false, "", 3000);
  }

  //Mail with contact
  sendEmail(){
    window.open(`mailto:${this.contact.email}`, "_system");
  }


}
