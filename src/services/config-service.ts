import { Injectable } from "@angular/core";
import { Headers, RequestOptions } from "@angular/http";
import { ToastController } from "ionic-angular/components/toast/toast-controller";
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class ConfigService {

    apiRoot: string;
    headers: Headers;
    options: RequestOptions;


    constructor(private toastCtrl: ToastController){
        // Project's Root API, Http headers,options
        this.apiRoot = "https://randomuser.me/api/";
        this.headers = new Headers({'Content-Type': 'application/json', 'Accept':'application/json'});
        this.options = new RequestOptions({ headers: this.headers });
    }

   
    // toaster for UI
    presentToast(message: string, classType: string, position:string, showCloseButton:boolean, closeButtonText: string, time: number) {
        let toast = this.toastCtrl.create({
          message: message,
          position: position,
          showCloseButton: showCloseButton,
          closeButtonText: closeButtonText,
          cssClass: classType,
          duration: time,
          dismissOnPageChange: false
        });
        toast.present();
    }


}