import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import 'rxjs/Rx';

import { ConfigService } from "./config-service";
import { Contact } from "../models/contact";
import { LoadingController } from "ionic-angular";


@Injectable()
export class ContactsService {

    contacts: Array<Contact>;

    constructor(private http: Http,
                private loadingCtrl: LoadingController,
                private configService: ConfigService){
    }


    // *************************************** Getting randomly generated collegues ********************************* // 
    getMyCollegues(isShow: boolean){
        let loader = this.loadingCtrl.create();
        isShow ? loader.present() : false;

        let promise = new Promise((resolve, reject) => {
            let apiURL = `${this.configService.apiRoot}?results=300`;
            this.http.get(apiURL,this.configService.options)
            .toPromise()
            .then((res) => {
                    loader.dismiss();
                    this.contacts = res.json().results;
                    this.contacts.forEach((contact, index) => {
                        this.contacts[index].name.first = contact.name.title + " " + contact.name.first + " " + contact.name.last;
                    });
                    resolve(res.json());
                },(msg) => {
                    loader.dismiss();
                    reject(msg.json());
                }
            );
        });
        return promise;
    }



}